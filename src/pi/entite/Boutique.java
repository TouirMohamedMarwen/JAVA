/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pi.entite;

/**
 *
 * @author DOMICILE
 */
public class Boutique {
    
     private int idBoutique;
     private String libelleBoutique;
     private String categorieBoutique;
     private String descriptionBoutique;
    private String imageBoutique;
    private String telephone;
    private String email ;

    public Boutique() {
    }

    public Boutique(int idBoutique,String libelleBoutique,String categorieBoutique,String descriptionBoutique,String imageBoutique,String telephone,String email) {
        this.idBoutique = idBoutique;
        this.libelleBoutique = libelleBoutique;
        this.categorieBoutique = categorieBoutique;
        this.descriptionBoutique = descriptionBoutique;
        this.imageBoutique = imageBoutique;
        this.telephone = telephone;
         this.email = email;
    }

    public int getIdBoutique() {
        return idBoutique;
    }

    @Override
    public String toString() {
        return "Boutique{" + "idBoutique=" + idBoutique + ", libelleBoutique=" + libelleBoutique + ", categorieBoutique=" + categorieBoutique + ", descriptionBoutique=" + descriptionBoutique + ", imageBoutique=" + imageBoutique + ", telephone=" + telephone + ", email=" + email + '}';
    }

    public void setIdBoutique(int idBoutique) {
        this.idBoutique = idBoutique;
    }

    public void setLibelleBoutique(String libelleBoutique) {
        this.libelleBoutique = libelleBoutique;
    }

    public void setCategorieBoutique(String categorieBoutique) {
        this.categorieBoutique = categorieBoutique;
    }

    public void setDescriptionBoutique(String descriptionBoutique) {
        this.descriptionBoutique = descriptionBoutique;
    }

    public void setImageBoutique(String imageBoutique) {
        this.imageBoutique = imageBoutique;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLibelleBoutique() {
        return libelleBoutique;
    }

    public String getCategorieBoutique() {
        return categorieBoutique;
    }

    public String getDescriptionBoutique() {
        return descriptionBoutique;
    }

    public String getImageBoutique() {
        return imageBoutique;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    
}
