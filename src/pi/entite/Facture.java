/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pi.entite;

/**
 *
 * @author DOMICILE
 */
public class Facture {
    
     private String id_facture;
     private String id_article;
     private float prix_HTC;
     private float tva;
    private String date_facture;
    private float prix_net;

    public Facture() {
    }

    public Facture(String id_facture,String id_article,float prix_HTC,float tva,String date_facture,float prix_net) {
        this.id_facture = id_facture;
        this.id_article = id_article;
        this.prix_HTC = prix_HTC;
        this.tva = tva;
        this.date_facture = date_facture;
        this.prix_net = prix_net;
    }

    public String getId_facture() {
        return id_facture;
    }
   public String getId_article() {
        return id_article;
    }
    public float getPrixHtc() {
        return prix_HTC;
    }
    public float getTva() {
        return tva;
    }
     public String getDate_facture() {
        return date_facture;
    }
      public float getPrix_net() {
        return prix_net;
    }
    public void setId_facture(String id_facture) {
        this.id_facture = id_facture;
    }
    public void setId_article(String id_article) {
        this.id_article = id_article;
    }
    public void setPrixHtc(float prix_HTC) {
        this.prix_HTC = prix_HTC;
    }
    public void setTva(float tva) {
        this.tva = tva;
    }
    public void setDate_facture(String date_facture) {
        this.date_facture = date_facture;
    }
     public void setPrix_net(float prix_net) {
        this.prix_net = prix_net ;
    }
    @Override
    public String toString() {
        return "Facture{" + "id_facture=" + id_facture + ",id_article=" +id_article +",prix HTC="+prix_HTC+",tvz="+tva+",date facture="+date_facture+",prix nrt="+prix_net+ '}';
    }

    public void getId_facture(String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
