/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.entite;

import pi.dao.interfaces.*;
import java.util.*;
import pi.entite.Article;
import pi.dao.classes.ArticleDAO;

/**
 *
 * @author Seyf
 * @param <Article>
 */
public  interface DAO<Article> {
    public void insert(Article t);
    public void delete(int id);
    public void modifier(Article article);
    public List<Article> displayAll();
    public Article findById(int id);
    
}
