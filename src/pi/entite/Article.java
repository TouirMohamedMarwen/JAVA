package pi.entite;

import java.sql.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Seyf
 */
public class Article {
    
    private int idProduit;
    private String nomProduit;
    private int idCatalogue;
    private int idPromotion;
    private String libelle;
    private String reference;
    private String description;
    private Float tva;
    private Float prix;
    private Date date;
    private String categorie;
      
    

    public Article() {
    }
    
    
    public Article (int idProduit,String nomProduit, int idCatalogue, int idPromotion, String libelle, String reference ,
            String description, Float tva, Float prix, Date date, String categorie)
    {
     this.idProduit=idProduit;  
     this.nomProduit=nomProduit;
     this.idCatalogue=idCatalogue;
     this.idPromotion=idPromotion;
     this.libelle=libelle;
     this.reference=reference;
     this.description=description;
     this.tva=tva;
     this.prix=prix;
     this.date=date;
     this.categorie=categorie;
    }
    
    public Article (String nomProduit, int idCatalogue, int idPromotion, String libelle, String reference ,
            String description, Float tva, Float prix, String categorie)
    {
      
     this.nomProduit=nomProduit;
     this.idCatalogue=idCatalogue;
     this.idPromotion=idPromotion;
     this.libelle=libelle;
     this.reference=reference;
     this.description=description;
     this.tva=tva;
     this.prix=prix;
     this.categorie=categorie;
    }
    
    

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public int getIdProduit() {
        return idProduit;
    }

    public int getIdCatalogue() {
        return idCatalogue;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getReference() {
        return reference;
    }

    public String getDescription() {
        return description;
    }

    public Float getTva() {
        return tva;
    }

    public Float getPrix() {
        return prix;
    }

    public Date getDate() {
        return date;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }

    public void setIdCatalogue(int idCatalogue) {
        this.idCatalogue = idCatalogue;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTva(Float tva) {
        this.tva = tva;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "Article{" + "idProduit=" + idProduit + ", nomProduit=" + nomProduit + ", idCatalogue=" + idCatalogue + ", idPromotion=" + idPromotion + ", libelle=" + libelle + ", reference=" + reference + ", description=" + description + ", tva=" + tva + ", prix=" + prix + ", date=" + date + ", categorie=" + categorie + '}';
    }

    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.idProduit;
        hash = 53 * hash + this.idCatalogue;
        hash = 53 * hash + this.idPromotion;
        return hash;
    }

    

    public int getIdcatalogue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
    
}

    
    
    