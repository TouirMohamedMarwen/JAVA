/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.entite;

import java.sql.Date;
import javax.swing.ImageIcon;

/**
 *
 * @author MARWA
 */
public class Compte {

 
    private int id;
    private String cin;
    private String nom;
    private String prenom;
    private String sexe;
    private Date Anniversaire;
    private String email;
    private String password;
    private String role;
    private int   telephone;
    private String adresse;
    private String image;
    private int idEnseigne;
  
    private int attente;
    private int bloquer;
    private int supprimer;

    public int getIdEnseigne() {
        return idEnseigne;
    }

    public void setIdEnseigne(int idEnseigne) {
        this.idEnseigne = idEnseigne;
    }
    
    
    
    public Compte(){
        
    }
    public Compte(String cin,String password){
        this.cin=cin;
        this.password=password;
    }
    public Compte(int id, String cin, String nom, String prenom, String sexe, Date Anniversaire, String email, 
            String password,  String role, int telephone, String adresse,String image,int idEnseigne
    ,int attente,int bloquer,int supprimer){
        
        this.id = id;
        this.cin=cin;
        this.nom=nom;
        this.prenom=prenom;
        this.sexe=sexe;
        this.Anniversaire=Anniversaire;
        this.email=email;
        this.password=password;
        this.role=role;
        this.telephone=telephone;
        this.adresse=adresse;
        
    
    
}
    
    public Compte(String cin, String nom, String prenom, String sexe, Date Anniversaire, String email, 
            String password,  String role, int telephone, String adresse){
        
        
        this.cin=cin;
        this.nom=nom;
        this.prenom=prenom;
        this.sexe=sexe;
        this.Anniversaire=Anniversaire;
        this.email=email;
        this.password=password;
        this.role=role;
        this.telephone=telephone;
        this.adresse=adresse;
        
    
    
}
    
    public Compte(int id,String cin, String nom, String prenom, String sexe, Date Anniversaire, String email, 
            String password,  String role, int telephone, String adresse){
        
        this.id=id;
        this.cin=cin;
        this.nom=nom;
        this.prenom=prenom;
        this.sexe=sexe;
        this.Anniversaire=Anniversaire;
        this.email=email;
        this.password=password;
        this.role=role;
        this.telephone=telephone;
        this.adresse=adresse;
        
    
    
}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date getAnniversaire() {
        return Anniversaire;
    }

    public void setAnniversaire(Date Anniversaire) {
        this.Anniversaire = Anniversaire;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getAttente() {
        return attente;
    }

    public void setAttente(int attente) {
        this.attente = attente;
    }

    public int getBloquer() {
        return bloquer;
    }

    public void setBloquer(int bloquer) {
        this.bloquer = bloquer;
    }

    public int getSupprimer() {
        return supprimer;
    }

    public void setSupprimer(int supprimer) {
        this.supprimer = supprimer;
    }
    

  
   

}