/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.dao.interfaces;


import pi.entite.Compte;
import java.util.List;

/**
 *
 * @author MARWA
 */
public interface ICompteDAO<Compte> {
    
    void ajouterCompte(Compte cpte);
    void modifierCompte(Compte cpte);
    Boolean chercherCompte(Compte cpte);
    Compte chechercomptemodif(Compte cpte);
    public List<Compte> displayAll();
    void delete(int id);
    
    
    
}
