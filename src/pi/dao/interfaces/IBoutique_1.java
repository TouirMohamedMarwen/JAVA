/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pi.dao.interfaces;
import java.sql.ResultSet;
import java.sql.Statement;
import pi.entite.Boutique;
import java.util.List;


/**
 *
 * @author DOMICILE
 */
public interface IBoutique_1 {

    /**
     *
     * @param x     
     * @return      
     */
   ResultSet afficher_boutique(Statement x);
//   ResultSet afficher_combo(Statement x);
   ResultSet rechercher_boutique(Statement x,String id_facture);
      void ajouter_boutique(Boutique boutique);

    void modifier_boutique(Boutique boutique);

    void supprimer_boutique(String idBoutque);

    /**
     *
     * @param stm
     * @param RS
     * @param id_facture
     * @return
     */
   // IFacture rechercher_facture(Statement stm,ResultSet RS,String id_facture);

    List<IFacture> DisplayAllfactures();
}
