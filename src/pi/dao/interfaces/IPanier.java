/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.dao.interfaces;

/**
 *
 * @author muerto
 */
import java.sql.*;
import pi.entite.Panier;

public interface IPanier {
   ResultSet afficher_panier(Statement x);
   ResultSet afficher_article(Statement x);
   ResultSet afficher_client(Statement x);
   ResultSet afficher_panierP(Statement x);
   ResultSet rechercher_panier(Statement x,int id_panier);
   void ajouter_panier(Panier p);
   void modifier_panier(Panier p);
   void supprimer_panier(String id_panier);
   ResultSet recherche_article(Statement x,String marque);
   
   
   
    
}
