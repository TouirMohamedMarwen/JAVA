/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pi.dao.interfaces;
import java.sql.ResultSet;
import java.sql.Statement;
import pi.entite.Facture;
import java.util.List;


/**
 *
 * @author DOMICILE
 */
public interface IFacture {

    /**
     *
     * @param x     
     * @return      
     */
   ResultSet afficher_facure(Statement x);
   ResultSet afficher_combo(Statement x);
   ResultSet rechercher_facture(Statement x,String id_facture);
      void ajouter_facure(Facture facture);

    void modifier_facture(Facture facture);

    void supprimer_facture(String id_facture);

    /**
     *
     * @param stm
     * @param RS
     * @param id_facture
     * @return
     */
   // IFacture rechercher_facture(Statement stm,ResultSet RS,String id_facture);

    List<IFacture> DisplayAllfactures();
}
