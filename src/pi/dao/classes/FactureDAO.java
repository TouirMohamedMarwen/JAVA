/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pi.dao.classes;
import java.sql.Connection;
import pi.dao.interfaces.IFacture;
import pi.utils.BDConnexion;
import pi.entite.Facture;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.FilterOutputStream;



/**
 *
 * @author DOMICILE
 */
public class FactureDAO implements IFacture{
    
    private final  Connection connection;
    ResultSet RS ;
   // Statement stm;
    public FactureDAO() {
      
        connection = (BDConnexion.getInstance());
    //Statement stm;
    }

    
    @Override
    public ResultSet afficher_combo(Statement x)
    {
        ResultSet var = null;
       
        try {
            var=x.executeQuery("Select idproduit from produit");
        } catch (SQLException ex) {
            Logger.getLogger(FactureDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            return var;
        
    }
    public ResultSet afficher_facure(Statement x ) {
       ResultSet var = null;
       
        try {
            var=x.executeQuery("Select * from facture");
        } catch (SQLException ex) {
            Logger.getLogger(FactureDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            return var;
        
   
    }

    @Override
     public void ajouter_facure(Facture facture) {
        
        try {
            
            String req = "insert into facture (id_article,prix_HTC,tva,date_facture,prix_net) values (?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, facture.getId_article());
            ps.setFloat(2, facture.getPrixHtc());
            ps.setFloat(3, facture.getTva());
           ps.setString(4, facture.getDate_facture());
            ps.setFloat(5, facture.getPrix_net());
           
           // ps.execute(req);
             ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier_facture(Facture facture) {
           String requete = "update facture set id_article=?, prix_HTC=?, tva=?, date_facture=?, prix_net=? where id_facture=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setString(1, facture.getId_article());
            ps.setFloat(2, facture.getPrixHtc());
            ps.setFloat(3, facture.getTva());
           ps.setString(4, facture.getDate_facture());
            ps.setFloat(5, facture.getPrix_net());
             ps.setString(6, facture.getId_facture());
            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
        }
    }
    

    @Override
    public void supprimer_facture(String id_facture) {
           String requete = "delete from facture where id_facture=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setString(1, id_facture);
            ps.executeUpdate();
            System.out.println("Suppression effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }

   

    @Override
    public List<IFacture> DisplayAllfactures() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

   

    
    public ResultSet rechercher_facture(Statement x, String id_facture) {
         ResultSet var = null;
       
        try {
           
            var=x.executeQuery("Select * from facture where id_facture LIKE '%"+id_facture+"%'");
        } catch (SQLException ex) {
            Logger.getLogger(FactureDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            return var;
    }

   

   


   
   
}
