/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.dao.classes;

/**
 *
 * @author Seyf
 */

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pi.entite.*;
import pi.dao.interfaces.DAO;
import pi.utils.BDConnexion;


public class ArticleDAO implements DAO<Article> {

    public static ArticleDAO getInstance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    Connection connection;
    
    public ArticleDAO()
    {
        connection = BDConnexion.getInstance();
    }
    
    @Override
    public void insert(Article t) {
        String req = "INSERT INTO produit (idCatalogue, nomProduit, idPromotion, libelle, reference, description, tva, prix, date, categorie) VALUES (?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, t.getIdCatalogue());
            ps.setString(2, t.getNomProduit());
            ps.setInt(3, t.getIdPromotion());
            ps.setString(4, t.getLibelle());
            ps.setString(5, t.getReference());
            ps.setString(6, t.getDescription());
            ps.setFloat(7, t.getTva());
            ps.setFloat(8, t.getPrix());
            ps.setDate(9, t.getDate());
            ps.setString(10, t.getCategorie());
            ps.executeUpdate();
         
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        String req = "DELETE FROM produit WHERE idProduit=?";
 
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        int rowsDeleted = 0;
        try {
            rowsDeleted = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rowsDeleted > 0) {
            System.out.println("un porduit a ete supprimé!");
        }
    }
    
    @Override
    public void modifier(Article t) {
        String req = "UPDATE produit SET nomProduit=?, idCatalogue=?, idPromotion=?, libelle=?, reference=?, description=?, tva=?, prix=?, date=?, categorie=?  WHERE idProduit =?";
 
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(req);
            ps.setString(1, t.getNomProduit());
            ps.setInt(2, t.getIdCatalogue());
            ps.setInt(3, t.getIdPromotion());
            ps.setString(4, t.getLibelle());
            ps.setString(5, t.getReference());
            ps.setString(6, t.getDescription());
            ps.setFloat(7, t.getTva());
            ps.setFloat(8, t.getPrix());
            ps.setDate(9, t.getDate());
            ps.setString(10, t.getCategorie());
            ps.setInt(11, t.getIdProduit());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        int rowsUpdated = 0;
        try {
            rowsUpdated = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rowsUpdated > 0) {
            System.out.println("");
        }
    }
    
    @Override
    public List<Article> displayAll() {
        List<Article> res = new ArrayList<>();
        String req = "SELECT * FROM produit";
        
        Statement statement;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(req);
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            while(result.next())
            {
                Article t;t = new Article(result.getInt(1),
                        result.getString(2),
                        result.getInt(3),
                        result.getInt(4),
                        result.getString(5),
                        result.getString(6),
                        result.getString(7),
                        result.getFloat(8),
                        result.getFloat(9),
                        result.getDate(10),
                        result.getString(11));
                res.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
       }
        return res;
    }
    
    @Override
    public Article findById(int id) {
        Article t = null;
        String req = "SELECT * FROM article WHERE id = "+id;
        Statement statement;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(req);
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       try {
            while(result.next())
            {
                t = new Article(result.getInt(1),
                    result.getString(2),
                    result.getInt(3),
                    result.getInt(4),
                    result.getString(5),
                    result.getString(6),
                    result.getString(7),
                    result.getFloat(8),
                    result.getFloat(9),
                    result.getDate(10),
                    result.getString(11));
            }
       } catch (SQLException ex) {
           Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
       }
        return t;
    }
}