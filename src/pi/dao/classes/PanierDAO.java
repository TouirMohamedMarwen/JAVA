/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.dao.classes;

/**
 *
 * @author muerto
 */
import java.sql.Connection;
import pi.dao.interfaces.IPanier;
import pi.utils.BDConnexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pi.entite.Panier;

public class PanierDAO implements IPanier{
    
    private final Connection connection;
    ResultSet RS;
    public PanierDAO()
    {
    connection= (BDConnexion.getInstance());
    }

    @Override
    public ResultSet afficher_panier(Statement x) {
       ResultSet var =null;
       try {
       var=x.executeQuery("SELECT  idpanier, idclient, idproduit FROM panier");
       }
       catch (SQLException ex) {
    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null,ex);
    }
       return var;
    }
    
    

   
    public ResultSet rechercher_panier(Statement x, String id_panier) {
         ResultSet var =null;
       try {
       var=x.executeQuery("Select * from panier Where idpanier LIKE '%"+id_panier+"%'");
       }
       catch (SQLException ex) {
    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null,ex);
    }
       return var;
    }

    @Override
    public void ajouter_panier(Panier p) {
        try {
            
            String req = "insert into panier (idclient,idproduit) values (?,?)where(select idproduit from produit where( panier.idproduit=produit.idproduit))";
            PreparedStatement ps = connection.prepareStatement(req);
           // ps.setInt(1, p.getId_article());
          
            
           
            ps.setInt(1, p.getId_article());
             ps.setInt(2, p.getId_client());
             
         
             ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    

    @Override
    public void modifier_panier(Panier p) {
     String req = "update panier set  idclient=?, idproduit where idpanier=?";
            try {
               PreparedStatement ps = connection.prepareStatement(req);
           // ps.setInt(1, p.getId_article());
          
            
           ps.setInt(1, p.getId_client());
            ps.setInt(2, p.getId_article());
             
             
             ps.setInt(3, p.getId_panier());
             
            
           
            
           
           // ps.execute(req);
             ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void supprimer_panier(String id_panier) {
        String req = "delete from panier where idpanier=?";
         try {
             PreparedStatement ps=connection.prepareStatement(req);
             ps.setString(1, id_panier);
             ps.executeUpdate();
           
         }
         catch (SQLException ex){
             System.out.println("erreur" + ex.getMessage() ) ;
         }
        
        
        
    }

    @Override
    public ResultSet afficher_article(Statement x) {
       ResultSet var =null;
       try {
       var=x.executeQuery("Select * from produit");
       }
       catch (SQLException ex) {
    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null,ex);
    }
       return var;
    }

    
    public ResultSet recherche_prix(Statement x, String id_article) {
        ResultSet var =null;
       try {
       var=x.executeQuery("Select prix from produit Where idProduit LIKE'"+id_article+"'");
       }
       catch (SQLException ex) {
    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null,ex);
    }
       return var;
    }
     public ResultSet recherche_article(Statement x, String marque) {
        ResultSet var =null;
       try {
       var=x.executeQuery("Select * from produit Where libelle LIKE'"+marque+"'");
       }
       catch (SQLException ex) {
    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null,ex);
    }
       return var;
    }

    @Override
    public ResultSet afficher_client(Statement x) {
        return null;
     
    }

    @Override
    public ResultSet afficher_panierP(Statement x) {
          ResultSet var =null;
       try {
       var=x.executeQuery("Select DISTINCT idpanier from panier");
       }
       catch (SQLException ex) {
    Logger.getLogger(PanierDAO.class.getName()).log(Level.SEVERE, null,ex);
    }
       return var;
    }

    @Override
    public ResultSet rechercher_panier(Statement x, int id_panier) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
