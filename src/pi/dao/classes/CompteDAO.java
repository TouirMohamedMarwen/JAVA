/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.dao.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.persistence.internal.core.helper.CoreClassConstants;
import pi.utils.BDConnexion;
import pi.dao.interfaces.ICompteDAO;
import pi.entite.Article;
import pi.entite.Compte;

/**
 *
 * @author Seyf
 */
public class CompteDAO implements ICompteDAO<Compte>{
 public static CompteDAO getInstance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    Connection connection;
    
    public CompteDAO()
    {
        connection = BDConnexion.getInstance();
    }

    @Override
    public void ajouterCompte(Compte cpte) {
        String req = "INSERT INTO user "
                + "(cin, nom, prenom, sexe, "
                + "Anniversaire, email, password, role, "
                + "telephone, adresse) VALUES (?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(req);
            ps.setString(1, cpte.getCin());
            ps.setString(2, cpte.getNom());
            ps.setString(3, cpte.getPrenom());
            ps.setString(4, cpte.getSexe());
            ps.setDate(5, cpte.getAnniversaire());
            ps.setString(6, cpte.getEmail());
            ps.setString(7, cpte.getPassword());
            ps.setString(8, cpte.getRole());
            ps.setInt(9, cpte.getTelephone());
            ps.setString(10, cpte.getAdresse());
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    @Override
    public void modifierCompte(Compte cpte) {
          String req = "UPDATE produit SET cin=?, nom=?, prenom=?, "
                  + "sexe=?, Anniversaire=?, "
                  + "Email=?, password=?, role=?, "
                  + "telephone=?, Adresse=?  WHERE id =?";
 
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(req);
            ps.setString(1, cpte.getCin());
            ps.setString(2, cpte.getNom());
            ps.setString(3, cpte.getPrenom());
            ps.setString(4, cpte.getSexe());
            ps.setDate(5, cpte.getAnniversaire());
            ps.setString(6, cpte.getEmail());
            ps.setString(7, cpte.getPassword());
            ps.setString(8, cpte.getRole());
            ps.setInt(9, cpte.getTelephone());
            ps.setString(10, cpte.getAdresse());
            ps.setInt(11, cpte.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        int rowsUpdated = 0;
        try {
            rowsUpdated = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rowsUpdated > 0) {
            System.out.println("");
        }
    }
    
    
    

    @Override
    public Boolean chercherCompte(Compte cpte) {
        Boolean conn = false;
        String req = "select cin , "
                + "password from user where "
                + "cin='"+cpte.getCin()+"' and password='"+cpte.getPassword()+"';";
        
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(req);
            ResultSet res = ps.executeQuery();
            while(res.next()){
                conn=true;
                
            }
            
        } catch (Exception e) {
        }
        if(conn==true){
            return true;
        }
        else{
            return false;
        }
        
    }

    @Override
    public Compte chechercomptemodif(Compte cpte){
            String req = "select cin , "
                + "password from user where "
                + "cin='"+cpte.getCin()+"' and password='"+cpte.getPassword()+"';";
        
        PreparedStatement ps;
     
        Compte cp = new Compte();
        try {
            
            ps = connection.prepareStatement(req);
            ResultSet res = ps.executeQuery();
            
            while(res.next()){
               
                cp.setCin(cpte.getCin());
                cp.setNom(cpte.getNom());
                cp.setPrenom(cpte.getPrenom());
                cp.setSexe(cpte.getSexe());
                cp.setAnniversaire(cpte.getAnniversaire());
                cp.setEmail(cpte.getEmail());
                cp.setPassword(cpte.getPassword());
                cp.setRole(cpte.getRole());
                cp.setTelephone(cpte.getTelephone());
                cp.setAdresse(cpte.getAdresse());
            }
                
            
            
        } catch (Exception e) {
        }
        return cp;
        
        
    }
        @Override
        public List<Compte> displayAll() {
    
        List<Compte> res = new ArrayList<>();
        String req = "SELECT id,cin,nom,prenom,sexe,anniversaire,email,password,role,telephone,adresse FROM user";
        
        Statement statement;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(req);
        } catch (SQLException ex) {
            
        }
        try {
            while(result.next())
            {
                Compte t;
                t = new Compte(result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getDate(6),
                        result.getString(7),
                        result.getString(8),
                        result.getString(9),
                        result.getInt(10),
                        result.getString(11));
                res.add(t);
                
            }
           
        } catch (SQLException ex) {
            
       }
        return res;
    }

    @Override
    public void delete(int id) {
        String req = "DELETE FROM user WHERE id=?";
 
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            
        }
        

        int rowsDeleted = 0;
        try {
            rowsDeleted = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CompteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (rowsDeleted > 0) {
            System.out.println("un porduit a ete supprimé!");
        }
    
    }




}
       
   

