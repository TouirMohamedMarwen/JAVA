/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pi.dao.classes;
import java.io.FilterOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pi.dao.interfaces.IBoutique;
import pi.dao.interfaces.IFacture;
import pi.entite.Boutique;
import pi.entite.Facture;
import pi.utils.BDConnexion;



/**
 *
 * @author DOMICILE
 */
public class BoutiqueDAO implements IBoutique{
    
    private final  Connection connection;
    ResultSet RS ;
   // Statement stm;
    public BoutiqueDAO() {
      
        connection = (BDConnexion.getInstance());
    //Statement stm;
    }

    @Override
    public ResultSet afficher_boutique(Statement x) {
       ResultSet var = null;
       
        try {
            var=x.executeQuery("Select * from boutique");
        } catch (SQLException ex) {
            Logger.getLogger(BoutiqueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            return var;
         //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet rechercher_boutique(Statement x, String idBoutique) {
        ResultSet var = null;
       
        try {
           
            var=x.executeQuery("Select * from boutique where idBoutique LIKE '%"+idBoutique+"%'");
        } catch (SQLException ex) {
            Logger.getLogger(BoutiqueDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            return var; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void ajouter_boutique(Boutique boutique) {
        try {
            
            String req = "insert into boutique (idBoutique,libelleBoutique,categorieBoutique,descriptionBoutique,imageBoutique,telephone,email) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, boutique.getIdBoutique());
            ps.setString(2, boutique.getLibelleBoutique());
            ps.setString(3, boutique.getCategorieBoutique());
           ps.setString(4, boutique.getDescriptionBoutique());
            ps.setString(5, boutique.getImageBoutique());
            ps.setString(6, boutique.getTelephone());
            ps.setString(7, boutique.getEmail());
           
           // ps.execute(req);
             ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BoutiqueDAO.class.getName()).log(Level.SEVERE, null, ex);
        } //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void modifier_boutique(Boutique boutique) {
       String requete = "update boutique set libelleBoutique=?, categorieBoutique=?, descriptionBoutique=?, imageBoutique=?, telephone=?, email=? where idBoutique=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
           
            ps.setString(1, boutique.getLibelleBoutique());
            ps.setString(2, boutique.getCategorieBoutique());
           ps.setString(3, boutique.getDescriptionBoutique());
            ps.setString(4, boutique.getImageBoutique());
            ps.setString(5, boutique.getTelephone());
            ps.setString(6, boutique.getEmail());
            ps.setInt(7, boutique.getIdBoutique());
            ps.executeUpdate();
            System.out.println("Mise à jour effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la mise à jour " + ex.getMessage());
    }}

    @Override
    public void supprimer_boutique(String idBoutique) {
        String requete = "delete from boutique where idBoutique=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setString(1, idBoutique);
            ps.executeUpdate();
            System.out.println("Suppression effectuée avec succès");
        } catch (SQLException ex) {
            System.out.println("erreur lors de la suppression " + ex.getMessage());
        }
    }

    @Override
    public List<IFacture> DisplayAllfactures() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
   

   

   


   
   
}
