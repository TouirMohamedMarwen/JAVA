/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pi.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;






import java.sql.DriverManager;
import java.sql.SQLException;


public class BDConnexion {
    
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String url = "jdbc:mysql://127.0.0.1:3306/pi2";
    // Connection Hello = DriverManager.getConnection("jdbc:mysql://YOUR-URL-HERE", "USERNAME", "PASSWORD");
    private static final String login = "root";
    private static final String pwd = "";
    private  static Connection connection;
    
    public BDConnexion () {}

    public Connection etablirConnection () {
        try{
            Class.forName(driver);
            connection = (Connection) DriverManager.getConnection(url, login, pwd);
            System.out.println("connexion établie");
        }

           catch (ClassNotFoundException ex) {
               System.out.println("Erreur de chargement de driver"+ex.getMessage());
            } catch (SQLException ex) {

               System.out.println("probleme d'établissement de connection"+ex.getMessage());
            }
            return connection;

    }

    public static Connection getInstance () {

        if (connection==null){
            new BDConnexion().etablirConnection();
        }

        return connection;
        
    }}
    
    
    
   
    

      
